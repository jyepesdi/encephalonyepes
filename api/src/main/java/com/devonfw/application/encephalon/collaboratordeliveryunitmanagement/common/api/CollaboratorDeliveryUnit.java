package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.common.api;

import java.sql.Date;
import java.sql.Timestamp;

import com.devonfw.application.encephalon.general.common.api.ApplicationEntity;

public interface CollaboratorDeliveryUnit extends ApplicationEntity {

  /**
   * getter for collaboratorId attribute
   *
   * @return collaboratorId
   */

  public Long getCollaboratorId();

  /**
   * @param collaborator setter for collaborator attribute
   */

  public void setCollaboratorId(Long collaboratorId);

  /**
   * getter for deliveryunitId attribute
   *
   * @return deliveryunitId
   */

  public Long getDeliveryunitId();

  /**
   * @param deliveryunit setter for deliveryunit attribute
   */

  public void setDeliveryunitId(Long deliveryunitId);

  /**
   * @return begin_dateId
   */

  public Date getBegin_date();

  /**
   * @param begin_date setter for begin_date attribute
   */

  public void setBegin_date(Date begin_date);

  /**
   * @return end_dateId
   */

  public Date getEnd_date();

  /**
   * @param end_date setter for end_date attribute
   */

  public void setEnd_date(Date end_date);

  /**
   * @return activeId
   */

  public boolean isActive();

  /**
   * @param active setter for active attribute
   */

  public void setActive(boolean active);

  /**
   * @return create_dateId
   */

  public Timestamp getCreate_date();

  /**
   * @param create_date setter for create_date attribute
   */

  public void setCreate_date(Timestamp create_date);

  /**
   * @return create_userId
   */

  public String getCreate_user();

  /**
   * @param create_user setter for create_user attribute
   */

  public void setCreate_user(String create_user);

  /**
   * @return modif_dateId
   */

  public Timestamp getModif_date();

  /**
   * @param modif_date setter for modif_date attribute
   */

  public void setModif_date(Timestamp modif_date);

  /**
   * @return modif_userId
   */

  public String getModif_user();

  /**
   * @param modif_user setter for modif_user attribute
   */

  public void setModif_user(String modif_user);

}
