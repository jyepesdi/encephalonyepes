package com.devonfw.application.encephalon.deliveryunitmanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.Deliveryunitmanagement;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitEto;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitSearchCriteriaTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Deliveryunitmanagement}.
 */
@Path("/deliveryunitmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface DeliveryunitmanagementRestService {

  /**
   * Delegates to {@link Deliveryunitmanagement#findDeliveryUnit}.
   *
   * @param id the ID of the {@link DeliveryUnitEto}
   * @return the {@link DeliveryUnitEto}
   */
  @GET
  @Path("/deliveryunit/{id}/")
  public DeliveryUnitEto getDeliveryUnit(@PathParam("id") long id);

  /**
   * Delegates to {@link Deliveryunitmanagement#saveDeliveryUnit}.
   *
   * @param deliveryunit the {@link DeliveryUnitEto} to be saved
   * @return the recently created {@link DeliveryUnitEto}
   */
  @POST
  @Path("/deliveryunit/")
  public DeliveryUnitEto saveDeliveryUnit(DeliveryUnitEto deliveryunit);

  /**
   * Delegates to {@link Deliveryunitmanagement#deleteDeliveryUnit}.
   *
   * @param id ID of the {@link DeliveryUnitEto} to be deleted
   */
  @DELETE
  @Path("/deliveryunit/{id}/")
  public void deleteDeliveryUnit(@PathParam("id") long id);

  /**
   * Delegates to {@link Deliveryunitmanagement#findDeliveryUnitEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding deliveryunits.
   * @return the {@link Page list} of matching {@link DeliveryUnitEto}s.
   */
  @Path("/deliveryunit/search")
  @POST
  public Page<DeliveryUnitEto> findDeliveryUnits(DeliveryUnitSearchCriteriaTo searchCriteriaTo);

}