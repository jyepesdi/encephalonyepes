package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.usecase.UcFindCollaboratorDeliveryUnit;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.usecase.UcManageCollaboratorDeliveryUnit;

/**
 * Interface for Collaboratordeliveryunitmanagement component.
 */
public interface Collaboratordeliveryunitmanagement
    extends UcFindCollaboratorDeliveryUnit, UcManageCollaboratorDeliveryUnit {

}
