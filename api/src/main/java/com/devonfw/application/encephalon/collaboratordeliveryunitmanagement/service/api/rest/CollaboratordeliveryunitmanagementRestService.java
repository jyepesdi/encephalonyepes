package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.Collaboratordeliveryunitmanagement;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitCto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitEto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitSearchCriteriaTo;

/**
 * The service interface for REST calls in order to execute the logic of component
 * {@link Collaboratordeliveryunitmanagement}.
 */
@Path("/collaboratordeliveryunitmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface CollaboratordeliveryunitmanagementRestService {

  /**
   * Delegates to {@link Collaboratordeliveryunitmanagement#findCollaboratorDeliveryUnitCto}.
   *
   * @param id the ID of the {@link CollaboratorDeliveryUnitCto}
   * @return the {@link CollaboratorDeliveryUnitCto}
   */
  @GET
  @Path("/collaboratordeliveryunit/cto/{id}/")
  public CollaboratorDeliveryUnitCto getCollaboratorDeliveryUnitCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Collaboratordeliveryunitmanagement#findCollaboratorDeliveryUnitCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding collaboratordeliveryunits.
   * @return the {@link Page list} of matching {@link CollaboratorDeliveryUnitCto}s.
   */
  @Path("/collaboratordeliveryunit/cto/search")
  @POST
  public Page<CollaboratorDeliveryUnitCto> findCollaboratorDeliveryUnitCtos(
      CollaboratorDeliveryUnitSearchCriteriaTo searchCriteriaTo);

  /**
   * @param id
   * @return
   */
  CollaboratorDeliveryUnitEto getCollaboratorDeliveryUnit(long id);

  /**
   * @param collaboratordeliveryunit
   * @return
   */
  CollaboratorDeliveryUnitEto saveCollaboratorDeliveryUnit(CollaboratorDeliveryUnitEto collaboratordeliveryunit);

  /**
   * @param id
   */
  void deleteCollaboratorDeliveryUnit(long id);

  /**
   * @param searchCriteriaTo
   * @return
   */
  Page<CollaboratorDeliveryUnitEto> findCollaboratorDeliveryUnits(
      CollaboratorDeliveryUnitSearchCriteriaTo searchCriteriaTo);

}
