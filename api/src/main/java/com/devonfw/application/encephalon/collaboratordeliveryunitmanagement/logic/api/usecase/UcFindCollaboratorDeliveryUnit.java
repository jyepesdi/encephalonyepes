package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.usecase;

import java.util.List;

import org.springframework.data.domain.Page;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitCto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitEto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitSearchCriteriaTo;

public interface UcFindCollaboratorDeliveryUnit {

  /**
   * Returns a composite CollaboratorDeliveryUnit by its id 'id'.
   *
   * @param id The id 'id' of the CollaboratorDeliveryUnit.
   * @return The {@link CollaboratorDeliveryUnitCto} with id 'id'
   */
  CollaboratorDeliveryUnitCto findCollaboratorDeliveryUnitCto(long id);

  /**
   * Returns a paginated list of composite CollaboratorDeliveryUnits matching the search criteria.
   *
   * @param criteria the {@link CollaboratorDeliveryUnitSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CollaboratorDeliveryUnitCto}s.
   */
  Page<CollaboratorDeliveryUnitCto> findCollaboratorDeliveryUnitCtos(CollaboratorDeliveryUnitSearchCriteriaTo criteria);

  /**
   * @param id
   * @return
   */
  CollaboratorDeliveryUnitEto findCollaboratorDeliveryUnit(long id);

  /**
   * @param criteria
   * @return
   */
  Page<CollaboratorDeliveryUnitEto> findCollaboratorDeliveryUnits(CollaboratorDeliveryUnitSearchCriteriaTo criteria);

}
