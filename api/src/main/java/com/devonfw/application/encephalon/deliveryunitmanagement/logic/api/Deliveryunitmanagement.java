package com.devonfw.application.encephalon.deliveryunitmanagement.logic.api;

import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.usecase.UcFindDeliveryUnit;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.usecase.UcManageDeliveryUnit;

/**
 * Interface for Deliveryunitmanagement component.
 */
public interface Deliveryunitmanagement extends UcFindDeliveryUnit, UcManageDeliveryUnit {

}
