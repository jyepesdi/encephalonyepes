package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to;

import com.devonfw.application.encephalon.collaboratormanagement.logic.api.to.CollaboratorEto;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitEto;
import com.devonfw.module.basic.common.api.to.AbstractCto;

/**
 * Composite transport object of CollaboratorDeliveryUnit
 */
public class CollaboratorDeliveryUnitCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private CollaboratorDeliveryUnitEto collaboratorDeliveryUnit;

  private CollaboratorEto collaborator;

  private DeliveryUnitEto deliveryunit;

  public CollaboratorDeliveryUnitEto getCollaboratorDeliveryUnit() {

    return collaboratorDeliveryUnit;
  }

  public void setCollaboratorDeliveryUnit(CollaboratorDeliveryUnitEto collaboratorDeliveryUnit) {

    this.collaboratorDeliveryUnit = collaboratorDeliveryUnit;
  }

  public CollaboratorEto getCollaborator() {

    return collaborator;
  }

  public void setCollaborator(CollaboratorEto collaborator) {

    this.collaborator = collaborator;
  }

  public DeliveryUnitEto getDeliveryunit() {

    return deliveryunit;
  }

  public void setDeliveryunit(DeliveryUnitEto deliveryunit) {

    this.deliveryunit = deliveryunit;
  }

}
