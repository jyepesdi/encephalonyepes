package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.common.api;

import com.devonfw.application.encephalon.general.common.api.ApplicationEntity;

/**
 * @author jyepesdi
 *
 */
public interface CustomCollaboratorDeliveryUnit extends ApplicationEntity {

  // crear getter y setters (Strings) abstractos

  // PEOPLE_NAME
  // DELIVERY_UNIT
  // BEGIN_DATE
  // END_DATE

  // Hacer un ETO y CTO de CustomCollaborarotrDeliveryUnit en Logic.api>to
  // En eto crear getter y setters de las 4 columnas de la tabla todo en string *fijarme en colaboratordeliveryuniteto
  // En cto crear getter y setter de CustomCollaboratorDeliveryUnitETO CollaboratorETO"+ "DeliveryUnitETO"

}
