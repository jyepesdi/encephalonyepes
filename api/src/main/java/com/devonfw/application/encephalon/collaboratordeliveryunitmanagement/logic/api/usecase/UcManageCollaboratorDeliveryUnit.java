package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.usecase;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitEto;

/**
 * Interface of UcManageCollaboratorDeliveryUnit to centralize documentation and signatures of methods.
 */
public interface UcManageCollaboratorDeliveryUnit {

  /**
   * Deletes a collaboratorDeliveryUnit from the database by its id 'collaboratorDeliveryUnitId'.
   *
   * @param collaboratorDeliveryUnitId Id of the collaboratorDeliveryUnit to delete
   * @return boolean <code>true</code> if the collaboratorDeliveryUnit can be deleted, <code>false</code> otherwise
   */
  boolean deleteCollaboratorDeliveryUnit(long collaboratorDeliveryUnitId);

  /**
   * Saves a collaboratorDeliveryUnit and store it in the database.
   *
   * @param collaboratorDeliveryUnit the {@link CollaboratorDeliveryUnitEto} to create.
   * @return the new {@link CollaboratorDeliveryUnitEto} that has been saved with ID and version.
   */
  CollaboratorDeliveryUnitEto saveCollaboratorDeliveryUnit(CollaboratorDeliveryUnitEto collaboratorDeliveryUnit);

}
