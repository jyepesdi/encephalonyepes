package com.devonfw.application.encephalon.deliveryunitmanagement.logic.impl.usecase;

import java.util.Optional;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.encephalon.deliveryunitmanagement.dataaccess.api.DeliveryUnitEntity;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitEto;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitSearchCriteriaTo;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.usecase.UcFindDeliveryUnit;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.base.usecase.AbstractDeliveryUnitUc;

/**
 * Use case implementation for searching, filtering and getting DeliveryUnits
 */
@Named
@Validated
@Transactional
public class UcFindDeliveryUnitImpl extends AbstractDeliveryUnitUc implements UcFindDeliveryUnit {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindDeliveryUnitImpl.class);

  @Override
  public DeliveryUnitEto findDeliveryUnit(long id) {

    LOG.debug("Get DeliveryUnit with id {} from database.", id);
    Optional<DeliveryUnitEntity> foundEntity = getDeliveryUnitRepository().findById(id);
    if (foundEntity.isPresent())
      return getBeanMapper().map(foundEntity.get(), DeliveryUnitEto.class);
    else
      return null;
  }

  @Override
  public Page<DeliveryUnitEto> findDeliveryUnits(DeliveryUnitSearchCriteriaTo criteria) {

    Page<DeliveryUnitEntity> deliveryunits = getDeliveryUnitRepository().findByCriteria(criteria);
    return mapPaginatedEntityList(deliveryunits, DeliveryUnitEto.class);
  }

}
