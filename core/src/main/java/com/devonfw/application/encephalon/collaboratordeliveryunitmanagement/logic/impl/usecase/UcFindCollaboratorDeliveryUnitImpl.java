package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.impl.usecase;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.dataaccess.api.CollaboratorDeliveryUnitEntity;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitCto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitEto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitSearchCriteriaTo;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.usecase.UcFindCollaboratorDeliveryUnit;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.base.usecase.AbstractCollaboratorDeliveryUnitUc;
import com.devonfw.application.encephalon.collaboratormanagement.logic.api.to.CollaboratorEto;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitEto;

/**
 * Use case implementation for searching, filtering and getting CollaboratorDeliveryUnits
 */
@Named
@Validated
@Transactional
public class UcFindCollaboratorDeliveryUnitImpl extends AbstractCollaboratorDeliveryUnitUc
    implements UcFindCollaboratorDeliveryUnit {

  /**
   * Logger instance.
   */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindCollaboratorDeliveryUnitImpl.class);

  @Override
  public CollaboratorDeliveryUnitEto findCollaboratorDeliveryUnit(long id) {

    LOG.debug("Get CollaboratorDeliveryUnit with id {} from database.", id);
    Optional<CollaboratorDeliveryUnitEntity> foundEntity = getCollaboratorDeliveryUnitRepository().findById(id);
    if (foundEntity.isPresent())
      return getBeanMapper().map(foundEntity.get(), CollaboratorDeliveryUnitEto.class);
    else
      return null;
  }

  @Override
  public Page<CollaboratorDeliveryUnitEto> findCollaboratorDeliveryUnits(
      CollaboratorDeliveryUnitSearchCriteriaTo criteria) {

    Page<CollaboratorDeliveryUnitEntity> collaboratordeliveryunits = getCollaboratorDeliveryUnitRepository()
        .findByCriteria(criteria);
    return mapPaginatedEntityList(collaboratordeliveryunits, CollaboratorDeliveryUnitEto.class);
  }

  @Override
  public CollaboratorDeliveryUnitCto findCollaboratorDeliveryUnitCto(long id) {

    LOG.debug("Get CollaboratorDeliveryUnitCto with id {} from database.", id);
    CollaboratorDeliveryUnitEntity entity = getCollaboratorDeliveryUnitRepository().find(id);
    CollaboratorDeliveryUnitCto cto = new CollaboratorDeliveryUnitCto();
    cto.setCollaboratorDeliveryUnit(getBeanMapper().map(entity, CollaboratorDeliveryUnitEto.class));
    cto.setCollaborator(getBeanMapper().map(entity.getCollaborator(), CollaboratorEto.class));
    cto.setDeliveryunit(getBeanMapper().map(entity.getDeliveryunit(), DeliveryUnitEto.class));

    return cto;
  }

  @Override
  public Page<CollaboratorDeliveryUnitCto> findCollaboratorDeliveryUnitCtos(
      CollaboratorDeliveryUnitSearchCriteriaTo criteria) {

    Page<CollaboratorDeliveryUnitEntity> collaboratordeliveryunits = getCollaboratorDeliveryUnitRepository()
        .findByCriteria(criteria);
    List<CollaboratorDeliveryUnitCto> ctos = new ArrayList<>();
    for (CollaboratorDeliveryUnitEntity entity : collaboratordeliveryunits.getContent()) {
      CollaboratorDeliveryUnitCto cto = new CollaboratorDeliveryUnitCto();
      cto.setCollaboratorDeliveryUnit(getBeanMapper().map(entity, CollaboratorDeliveryUnitEto.class));
      cto.setCollaborator(getBeanMapper().map(entity.getCollaborator(), CollaboratorEto.class));
      cto.setDeliveryunit(getBeanMapper().map(entity.getDeliveryunit(), DeliveryUnitEto.class));
      ctos.add(cto);
    }
    Pageable pagResultTo = PageRequest.of(criteria.getPageable().getPageNumber(), criteria.getPageable().getPageSize());

    return new PageImpl<>(ctos, pagResultTo, collaboratordeliveryunits.getTotalElements());
  }

}
