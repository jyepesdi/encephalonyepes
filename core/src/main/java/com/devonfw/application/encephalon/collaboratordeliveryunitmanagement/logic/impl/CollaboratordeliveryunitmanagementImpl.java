package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.Collaboratordeliveryunitmanagement;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitCto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitEto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitSearchCriteriaTo;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.usecase.UcFindCollaboratorDeliveryUnit;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.usecase.UcManageCollaboratorDeliveryUnit;
import com.devonfw.application.encephalon.general.logic.base.AbstractComponentFacade;

/**
 * Implementation of component interface of collaboratordeliveryunitmanagement
 */
@Named
public class CollaboratordeliveryunitmanagementImpl extends AbstractComponentFacade
    implements Collaboratordeliveryunitmanagement {

  @Inject
  private UcFindCollaboratorDeliveryUnit ucFindCollaboratorDeliveryUnit;

  @Inject
  private UcManageCollaboratorDeliveryUnit ucManageCollaboratorDeliveryUnit;

  @Override
  public CollaboratorDeliveryUnitEto findCollaboratorDeliveryUnit(long id) {

    return this.ucFindCollaboratorDeliveryUnit.findCollaboratorDeliveryUnit(id);
  }

  @Override
  public Page<CollaboratorDeliveryUnitEto> findCollaboratorDeliveryUnits(
      CollaboratorDeliveryUnitSearchCriteriaTo criteria) {

    return this.ucFindCollaboratorDeliveryUnit.findCollaboratorDeliveryUnits(criteria);
  }

  @Override
  public CollaboratorDeliveryUnitEto saveCollaboratorDeliveryUnit(
      CollaboratorDeliveryUnitEto collaboratordeliveryunit) {

    return this.ucManageCollaboratorDeliveryUnit.saveCollaboratorDeliveryUnit(collaboratordeliveryunit);
  }

  @Override
  public boolean deleteCollaboratorDeliveryUnit(long id) {

    return this.ucManageCollaboratorDeliveryUnit.deleteCollaboratorDeliveryUnit(id);
  }

  @Override
  public CollaboratorDeliveryUnitCto findCollaboratorDeliveryUnitCto(long id) {

    return ucFindCollaboratorDeliveryUnit.findCollaboratorDeliveryUnitCto(id);
  }

  @Override
  public Page<CollaboratorDeliveryUnitCto> findCollaboratorDeliveryUnitCtos(
      CollaboratorDeliveryUnitSearchCriteriaTo criteria) {

    return ucFindCollaboratorDeliveryUnit.findCollaboratorDeliveryUnitCtos(criteria);
  }

}
