package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.base.usecase;

import javax.inject.Inject;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.dataaccess.api.repo.CollaboratorDeliveryUnitRepository;
import com.devonfw.application.encephalon.general.logic.base.AbstractUc;

/**
 * Abstract use case for CollaboratorDeliveryUnits, which provides access to the commonly necessary data access objects.
 */
public class AbstractCollaboratorDeliveryUnitUc extends AbstractUc {

  /** @see #getCollaboratorDeliveryUnitRepository() */
  @Inject
  private CollaboratorDeliveryUnitRepository collaboratorDeliveryUnitRepository;

  /**
   * Returns the field 'collaboratorDeliveryUnitRepository'.
   * 
   * @return the {@link CollaboratorDeliveryUnitRepository} instance.
   */
  public CollaboratorDeliveryUnitRepository getCollaboratorDeliveryUnitRepository() {

    return this.collaboratorDeliveryUnitRepository;
  }

}
