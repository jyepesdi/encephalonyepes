package com.devonfw.application.encephalon.deliveryunitmanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.Deliveryunitmanagement;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitEto;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitSearchCriteriaTo;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.usecase.UcFindDeliveryUnit;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.usecase.UcManageDeliveryUnit;
import com.devonfw.application.encephalon.general.logic.base.AbstractComponentFacade;

/**
 * Implementation of component interface of deliveryunitmanagement
 */
@Named
public class DeliveryunitmanagementImpl extends AbstractComponentFacade implements Deliveryunitmanagement {

  @Inject
  private UcFindDeliveryUnit ucFindDeliveryUnit;

  @Inject
  private UcManageDeliveryUnit ucManageDeliveryUnit;

  @Override
  public DeliveryUnitEto findDeliveryUnit(long id) {

    return this.ucFindDeliveryUnit.findDeliveryUnit(id);
  }

  @Override
  public Page<DeliveryUnitEto> findDeliveryUnits(DeliveryUnitSearchCriteriaTo criteria) {

    return this.ucFindDeliveryUnit.findDeliveryUnits(criteria);
  }

  @Override
  public DeliveryUnitEto saveDeliveryUnit(DeliveryUnitEto deliveryunit) {

    return this.ucManageDeliveryUnit.saveDeliveryUnit(deliveryunit);
  }

  @Override
  public boolean deleteDeliveryUnit(long id) {

    return this.ucManageDeliveryUnit.deleteDeliveryUnit(id);
  }
}
