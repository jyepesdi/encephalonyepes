package com.devonfw.application.encephalon.deliveryunitmanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.Deliveryunitmanagement;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitEto;
import com.devonfw.application.encephalon.deliveryunitmanagement.logic.api.to.DeliveryUnitSearchCriteriaTo;
import com.devonfw.application.encephalon.deliveryunitmanagement.service.api.rest.DeliveryunitmanagementRestService;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Deliveryunitmanagement}.
 */
@Named("DeliveryunitmanagementRestService")
public class DeliveryunitmanagementRestServiceImpl implements DeliveryunitmanagementRestService {

  @Inject
  private Deliveryunitmanagement deliveryunitmanagement;

  @Override
  public DeliveryUnitEto getDeliveryUnit(long id) {

    return this.deliveryunitmanagement.findDeliveryUnit(id);
  }

  @Override
  public DeliveryUnitEto saveDeliveryUnit(DeliveryUnitEto deliveryunit) {

    return this.deliveryunitmanagement.saveDeliveryUnit(deliveryunit);
  }

  @Override
  public void deleteDeliveryUnit(long id) {

    this.deliveryunitmanagement.deleteDeliveryUnit(id);
  }

  @Override
  public Page<DeliveryUnitEto> findDeliveryUnits(DeliveryUnitSearchCriteriaTo searchCriteriaTo) {

    return this.deliveryunitmanagement.findDeliveryUnits(searchCriteriaTo);
  }
}