package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.impl.usecase;

import java.util.Objects;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.dataaccess.api.CollaboratorDeliveryUnitEntity;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitEto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.usecase.UcManageCollaboratorDeliveryUnit;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.base.usecase.AbstractCollaboratorDeliveryUnitUc;

/**
 * Use case implementation for modifying and deleting CollaboratorDeliveryUnits
 */
@Named
@Validated
@Transactional
public class UcManageCollaboratorDeliveryUnitImpl extends AbstractCollaboratorDeliveryUnitUc
    implements UcManageCollaboratorDeliveryUnit {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcManageCollaboratorDeliveryUnitImpl.class);

  @Override
  public boolean deleteCollaboratorDeliveryUnit(long collaboratorDeliveryUnitId) {

    CollaboratorDeliveryUnitEntity collaboratorDeliveryUnit = getCollaboratorDeliveryUnitRepository()
        .find(collaboratorDeliveryUnitId);
    getCollaboratorDeliveryUnitRepository().delete(collaboratorDeliveryUnit);
    LOG.debug("The collaboratorDeliveryUnit with id '{}' has been deleted.", collaboratorDeliveryUnitId);
    return true;
  }

  @Override
  public CollaboratorDeliveryUnitEto saveCollaboratorDeliveryUnit(
      CollaboratorDeliveryUnitEto collaboratorDeliveryUnit) {

    Objects.requireNonNull(collaboratorDeliveryUnit, "collaboratorDeliveryUnit");

    CollaboratorDeliveryUnitEntity collaboratorDeliveryUnitEntity = getBeanMapper().map(collaboratorDeliveryUnit,
        CollaboratorDeliveryUnitEntity.class);

    // initialize, validate collaboratorDeliveryUnitEntity here if necessary
    CollaboratorDeliveryUnitEntity resultEntity = getCollaboratorDeliveryUnitRepository()
        .save(collaboratorDeliveryUnitEntity);
    LOG.debug("CollaboratorDeliveryUnit with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, CollaboratorDeliveryUnitEto.class);
  }
}
