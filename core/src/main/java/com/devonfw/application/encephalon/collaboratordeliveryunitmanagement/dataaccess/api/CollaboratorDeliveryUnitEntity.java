package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.dataaccess.api;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.common.api.CollaboratorDeliveryUnit;
import com.devonfw.application.encephalon.collaboratormanagement.dataaccess.api.CollaboratorEntity;
import com.devonfw.application.encephalon.deliveryunitmanagement.dataaccess.api.DeliveryUnitEntity;
import com.devonfw.application.encephalon.general.dataaccess.api.ApplicationPersistenceEntity;

@Entity
@Table(name = "`COLLABORATOR_DELIVERY_UNIT`", schema = "`Encephalon`")
public class CollaboratorDeliveryUnitEntity extends ApplicationPersistenceEntity implements CollaboratorDeliveryUnit {

  @ManyToOne
  @JoinColumn(name = "collaborator_id")
  private CollaboratorEntity collaborator;

  @ManyToOne
  @JoinColumn(name = "delunit_id")
  private DeliveryUnitEntity deliveryunit;

  @Column(name = "begin_date")
  private Date begin_date;

  @Column(name = "end_date")
  private Date end_date;

  @Column(name = "active")
  private boolean active;

  @Column(name = "create_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp create_date;

  @Column(name = "create_user")
  private String create_user;

  @Column(name = "modif_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp modif_date;

  @Column(name = "modif_user")
  private String modif_user;

  private static final long serialVersionUID = 1L;

  /**
   * @return collaborator
   */
  @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
  @JoinColumn(name = "collaborator_id")
  public CollaboratorEntity getCollaborator() {

    return this.collaborator;
  }

  /**
   * @param collaborator new value of {@link #getcollaborator}.
   */
  public void setCollaborator(CollaboratorEntity collaborator) {

    this.collaborator = collaborator;
  }

  /**
   * @return deliveryunit
   */
  @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
  @JoinColumn(name = "delunit_id")
  public DeliveryUnitEntity getDeliveryunit() {

    return this.deliveryunit;
  }

  /**
   * @param deliveryunit new value of {@link #getdeliveryunit}.
   */
  public void setDeliveryunit(DeliveryUnitEntity deliveryunit) {

    this.deliveryunit = deliveryunit;
  }

  /**
   * @return begin_date
   */
  public Date getBegin_date() {

    return this.begin_date;
  }

  /**
   * @param begin_date new value of {@link #getbegin_date}.
   */
  public void setBegin_date(Date begin_date) {

    this.begin_date = begin_date;
  }

  /**
   * @return end_date
   */
  public Date getEnd_date() {

    return this.end_date;
  }

  /**
   * @param end_date new value of {@link #getend_date}.
   */
  public void setEnd_date(Date end_date) {

    this.end_date = end_date;
  }

  /**
   * @return active
   */
  public boolean isActive() {

    return this.active;
  }

  /**
   * @param active new value of {@link #getactive}.
   */
  public void setActive(boolean active) {

    this.active = active;
  }

  /**
   * @return create_date
   */
  public Timestamp getCreate_date() {

    return this.create_date;
  }

  /**
   * @param create_date new value of {@link #getcreate_date}.
   */
  public void setCreate_date(Timestamp create_date) {

    this.create_date = create_date;
  }

  /**
   * @return create_user
   */
  public String getCreate_user() {

    return this.create_user;
  }

  /**
   * @param create_user new value of {@link #getcreate_user}.
   */
  public void setCreate_user(String create_user) {

    this.create_user = create_user;
  }

  /**
   * @return modif_date
   */
  public Timestamp getModif_date() {

    return this.modif_date;
  }

  /**
   * @param modif_date new value of {@link #getmodif_date}.
   */
  public void setModif_date(Timestamp modif_date) {

    this.modif_date = modif_date;
  }

  /**
   * @return modif_user
   */
  public String getModif_user() {

    return this.modif_user;
  }

  /**
   * @param modif_user new value of {@link #getmodif_user}.
   */
  public void setModif_user(String modif_user) {

    this.modif_user = modif_user;
  }

  @Override
  @Transient
  public Long getCollaboratorId() {

    if (this.collaborator == null) {
      return null;
    }
    return this.collaborator.getId();
  }

  @Override
  public void setCollaboratorId(Long collaboratorId) {

    if (collaboratorId == null) {
      this.collaborator = null;
    } else {
      CollaboratorEntity collaboratorEntity = new CollaboratorEntity();
      collaboratorEntity.setId(collaboratorId);
      this.collaborator = collaboratorEntity;
    }
  }

  @Override
  @Transient
  public Long getDeliveryunitId() {

    if (this.deliveryunit == null) {
      return null;
    }
    return this.deliveryunit.getId();
  }

  @Override
  public void setDeliveryunitId(Long deliveryunitId) {

    if (deliveryunitId == null) {
      this.deliveryunit = null;
    } else {
      DeliveryUnitEntity deliveryUnitEntity = new DeliveryUnitEntity();
      deliveryUnitEntity.setId(deliveryunitId);
      this.deliveryunit = deliveryUnitEntity;
    }
  }

}
