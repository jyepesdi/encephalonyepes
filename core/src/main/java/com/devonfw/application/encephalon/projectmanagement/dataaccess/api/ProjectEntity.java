package com.devonfw.application.encephalon.projectmanagement.dataaccess.api;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.devonfw.application.encephalon.deliveryunitmanagement.dataaccess.api.DeliveryUnitEntity;
import com.devonfw.application.encephalon.general.dataaccess.api.ApplicationPersistenceEntity;
import com.devonfw.application.encephalon.projectmanagement.common.api.Project;

/**
 * @author jyepesdi
 */
@Entity
@Table(name = "`PROJECT`", schema = "`Encephalon`")
public class ProjectEntity extends ApplicationPersistenceEntity implements Project {

  private DeliveryUnitEntity delivery;

  private String pon_code;

  private String short_desc;

  private String long_desc;

  private boolean activity;

  private boolean active;

  private Timestamp create_date;

  private Timestamp modif_date;

  private String create_user;

  private String modif_user;

  private static final long serialVersionUID = 1L;

  /**
   * @return delivery
   */

  @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
  @JoinColumn(name = "delunit_id")
  public DeliveryUnitEntity getDelivery() {

    return this.delivery;
  }

  /**
   * @param delivery new value of {@link #getdelivery}.
   */
  public void setDelivery(DeliveryUnitEntity delivery) {

    this.delivery = delivery;
  }

  /**
   * @return pon_code
   */
  @Override
  public String getPon_code() {

    return this.pon_code;
  }

  /**
   * @param pon_code new value of {@link #getpon_code}.
   */
  @Override
  public void setPon_code(String pon_code) {

    this.pon_code = pon_code;
  }

  /**
   * @return short_desc
   */
  @Override
  public String getShort_desc() {

    return this.short_desc;
  }

  /**
   * @param short_desc new value of {@link #getshort_desc}.
   */
  @Override
  public void setShort_desc(String short_desc) {

    this.short_desc = short_desc;
  }

  /**
   * @return long_desc
   */
  @Override
  public String getLong_desc() {

    return this.long_desc;
  }

  /**
   * @param long_desc new value of {@link #getlong_desc}.
   */
  @Override
  public void setLong_desc(String long_desc) {

    this.long_desc = long_desc;
  }

  /**
   * @return activity
   */
  @Override
  public boolean isActivity() {

    return this.activity;
  }

  /**
   * @param activity new value of {@link #getactivity}.
   */
  @Override
  public void setActivity(boolean activity) {

    this.activity = activity;
  }

  /**
   * @return active
   */
  @Override
  public boolean isActive() {

    return this.active;
  }

  /**
   * @param active new value of {@link #getactive}.
   */
  @Override
  public void setActive(boolean active) {

    this.active = active;
  }

  /**
   * @return create_date
   */
  @Override
  public Timestamp getCreate_date() {

    return this.create_date;
  }

  /**
   * @param create_date new value of {@link #getcreate_date}.
   */
  @Override
  public void setCreate_date(Timestamp create_date) {

    this.create_date = create_date;
  }

  /**
   * @return modif_date
   */
  @Override
  public Timestamp getModif_date() {

    return this.modif_date;
  }

  /**
   * @param modif_date new value of {@link #getmodif_date}.
   */
  @Override
  public void setModif_date(Timestamp modif_date) {

    this.modif_date = modif_date;
  }

  /**
   * @return create_user
   */
  @Override
  public String getCreate_user() {

    return this.create_user;
  }

  /**
   * @param create_user new value of {@link #getcreate_user}.
   */
  @Override
  public void setCreate_user(String create_user) {

    this.create_user = create_user;
  }

  /**
   * @return modif_user
   */
  @Override
  public String getModif_user() {

    return this.modif_user;
  }

  /**
   * @param modif_user new value of {@link #getmodif_user}.
   */
  @Override
  public void setModif_user(String modif_user) {

    this.modif_user = modif_user;
  }

  @Override
  @Transient
  public Long getDeliveryId() {

    if (this.delivery == null) {
      return null;
    }
    return this.delivery.getId();
  }

  @Override
  public void setDeliveryId(Long deliveryId) {

    if (deliveryId == null) {
      this.delivery = null;
    } else {
      DeliveryUnitEntity deliveryUnitEntity = new DeliveryUnitEntity();
      deliveryUnitEntity.setId(deliveryId);
      this.delivery = deliveryUnitEntity;
    }
  }

}
