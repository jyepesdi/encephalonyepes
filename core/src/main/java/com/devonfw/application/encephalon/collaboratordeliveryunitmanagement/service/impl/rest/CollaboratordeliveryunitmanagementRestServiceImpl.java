package com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;

import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.Collaboratordeliveryunitmanagement;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitCto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitEto;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.logic.api.to.CollaboratorDeliveryUnitSearchCriteriaTo;
import com.devonfw.application.encephalon.collaboratordeliveryunitmanagement.service.api.rest.CollaboratordeliveryunitmanagementRestService;

/**
 * The service implementation for REST calls in order to execute the logic of component
 * {@link Collaboratordeliveryunitmanagement}.
 */
@Named("CollaboratordeliveryunitmanagementRestService")
public class CollaboratordeliveryunitmanagementRestServiceImpl
    implements CollaboratordeliveryunitmanagementRestService {

  @Inject
  private Collaboratordeliveryunitmanagement collaboratordeliveryunitmanagement;

  @Override
  public CollaboratorDeliveryUnitEto getCollaboratorDeliveryUnit(long id) {

    return this.collaboratordeliveryunitmanagement.findCollaboratorDeliveryUnit(id);
  }

  @Override
  public CollaboratorDeliveryUnitEto saveCollaboratorDeliveryUnit(
      CollaboratorDeliveryUnitEto collaboratordeliveryunit) {

    return this.collaboratordeliveryunitmanagement.saveCollaboratorDeliveryUnit(collaboratordeliveryunit);
  }

  @Override
  public void deleteCollaboratorDeliveryUnit(long id) {

    this.collaboratordeliveryunitmanagement.deleteCollaboratorDeliveryUnit(id);
  }

  @Override
  public Page<CollaboratorDeliveryUnitEto> findCollaboratorDeliveryUnits(
      CollaboratorDeliveryUnitSearchCriteriaTo searchCriteriaTo) {

    return this.collaboratordeliveryunitmanagement.findCollaboratorDeliveryUnits(searchCriteriaTo);
  }

  @Override
  public CollaboratorDeliveryUnitCto getCollaboratorDeliveryUnitCto(long id) {

    return this.collaboratordeliveryunitmanagement.findCollaboratorDeliveryUnitCto(id);
  }

  @Override
  public Page<CollaboratorDeliveryUnitCto> findCollaboratorDeliveryUnitCtos(
      CollaboratorDeliveryUnitSearchCriteriaTo searchCriteriaTo) {

    return this.collaboratordeliveryunitmanagement.findCollaboratorDeliveryUnitCtos(searchCriteriaTo);
  }

}
